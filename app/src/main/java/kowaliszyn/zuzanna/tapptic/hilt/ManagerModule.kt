package kowaliszyn.zuzanna.tapptic.hilt

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kowaliszyn.zuzanna.tapptic.manager.*
import kowaliszyn.zuzanna.tapptic.manager.impl.*
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class ManagerModule {

    @Binds
    @Singleton
    abstract fun bindDataManager(dataManagerImpl: DataManagerImpl): DataManager

    @Binds
    @Singleton
    abstract fun bindNetworkManager(networkManagerImpl: NetworkManagerImpl): NetworkManager

    @Binds
    @Singleton
    abstract fun bindErrorManager(networkManagerImpl: ErrorManagerImpl): ErrorManager
}
