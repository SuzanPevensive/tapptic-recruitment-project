package kowaliszyn.zuzanna.tapptic.network

import kotlinx.coroutines.Deferred
import kowaliszyn.zuzanna.tapptic.const.PathsConst
import kowaliszyn.zuzanna.tapptic.model.response.dto.*
import retrofit2.http.*

interface Endpoints {

    @GET(PathsConst.PATH_LIST_ITEMS)
    fun getListItemsAsync(): Deferred<List<ItemResponseDto?>?>

    @GET(PathsConst.PATH_LIST_ITEMS)
    fun getItemAsync(@Query("name") itemName: String): Deferred<ItemResponseDto?>
}
