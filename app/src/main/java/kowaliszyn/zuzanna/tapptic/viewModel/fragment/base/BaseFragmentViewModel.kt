package kowaliszyn.zuzanna.tapptic.viewModel.fragment.base

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.SavedStateHandle
import kowaliszyn.zuzanna.tapptic.viewModel.base.BaseViewModel
import kowaliszyn.zuzanna.tapptic.viewModel.repository.base.BaseRepository

abstract class BaseFragmentViewModel<B : ViewDataBinding, out R : BaseRepository> constructor(
    repository: R,
    savedStateHandle: SavedStateHandle
) : BaseViewModel<B, R>(repository, savedStateHandle)
