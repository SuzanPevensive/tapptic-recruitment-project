package kowaliszyn.zuzanna.tapptic.viewModel.repository.activity

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import kowaliszyn.zuzanna.tapptic.converter.DataConverter
import kowaliszyn.zuzanna.tapptic.manager.DataManager
import kowaliszyn.zuzanna.tapptic.manager.NetworkManager
import kowaliszyn.zuzanna.tapptic.viewModel.repository.base.BaseRepository
import javax.inject.Inject

class MainActivityRepository @Inject constructor(
    @ApplicationContext context: Context,
    dataConverter: DataConverter,
    dataManager: DataManager,
    networkManager: NetworkManager
) : BaseRepository(
    context,
    dataConverter,
    dataManager,
    networkManager
) {

    private var chunk = 0

    suspend fun getListItemsSize() = dataManager.getAllItems().size

    suspend fun getListItemsNextChunk() = dataManager.getItems(chunk).also { chunk += 1 }

    suspend fun updateListItems() =
        networkManager.getListItems()?.let { listItems ->
            chunk = 0
            dataManager.clearAllData()
            dataManager.addItems(dataConverter.convertListItemsResponse(listItems))
        }
}
