package kowaliszyn.zuzanna.tapptic.viewModel.dialog

import androidx.lifecycle.SavedStateHandle
import dagger.hilt.android.lifecycle.HiltViewModel
import kowaliszyn.zuzanna.tapptic.databinding.DialogDetailsBinding
import kowaliszyn.zuzanna.tapptic.utils.event.impl.SingleClickEvent
import kowaliszyn.zuzanna.tapptic.viewModel.dialog.base.BaseDialogViewModel
import kowaliszyn.zuzanna.tapptic.viewModel.repository.dialog.DetailsDialogRepository
import javax.inject.Inject

@HiltViewModel
class DetailsDialogViewModel @Inject constructor(
    repositoryDetailsDialog: DetailsDialogRepository,
    savedStateHandle: SavedStateHandle
) : BaseDialogViewModel<DialogDetailsBinding, DetailsDialogRepository>(
    repositoryDetailsDialog,
    savedStateHandle
) {
    fun closeDialog(event: SingleClickEvent) {
        closeDialogEvent.run()
        event.done()
    }
}
