package kowaliszyn.zuzanna.tapptic.viewModel.repository.activity

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import kowaliszyn.zuzanna.tapptic.converter.DataConverter
import kowaliszyn.zuzanna.tapptic.manager.DataManager
import kowaliszyn.zuzanna.tapptic.manager.NetworkManager
import kowaliszyn.zuzanna.tapptic.viewModel.repository.base.BaseRepository
import javax.inject.Inject

class SplashActivityRepository @Inject constructor(
    @ApplicationContext context: Context,
    dataConverter: DataConverter,
    dataManager: DataManager,
    networkManager: NetworkManager
) : BaseRepository(context, dataConverter, dataManager, networkManager) {

    suspend fun clearAllData() = dataManager.clearAllData()
}
