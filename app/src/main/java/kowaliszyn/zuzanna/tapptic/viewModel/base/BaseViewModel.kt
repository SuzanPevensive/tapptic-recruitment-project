package kowaliszyn.zuzanna.tapptic.viewModel.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import kowaliszyn.zuzanna.tapptic.BR
import kowaliszyn.zuzanna.tapptic.viewModel.repository.base.BaseRepository

abstract class BaseViewModel<B : ViewDataBinding, out R : BaseRepository>(
    protected val repository: R,
    protected val savedStateHandle: SavedStateHandle
) : ViewModel() {

    protected var binding: B? = null
    protected val context get() = repository.context
    val rootView get() = binding?.root

    open fun init(
        lifecycleOwner: LifecycleOwner?,
        layoutId: Int,
        inflater: LayoutInflater,
        container: ViewGroup? = null
    ) {
        binding = DataBindingUtil.inflate<B>(inflater, layoutId, container, false).apply {
            setVariable(BR.viewModel, this@BaseViewModel)
        }
        lifecycleOwner?.let { joinToLifeCycle(it) }
    }

    open fun joinToLifeCycle(lifecycleOwner: LifecycleOwner) {
        binding?.lifecycleOwner = lifecycleOwner
    }

    open fun handleError(error: Throwable?) {}

    open fun onSaveState() {}
}
