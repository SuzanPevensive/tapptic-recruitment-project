package kowaliszyn.zuzanna.tapptic.viewModel.fragment

import androidx.core.content.ContextCompat
import androidx.lifecycle.SavedStateHandle
import com.squareup.picasso.Picasso
import dagger.hilt.android.lifecycle.HiltViewModel
import kowaliszyn.zuzanna.tapptic.R
import kowaliszyn.zuzanna.tapptic.databinding.FragmentDetailsBinding
import kowaliszyn.zuzanna.tapptic.utils.extension.global.onMain
import kowaliszyn.zuzanna.tapptic.utils.extension.global.onScope
import kowaliszyn.zuzanna.tapptic.utils.extension.loadInto
import kowaliszyn.zuzanna.tapptic.utils.liveData.ScopedLiveData
import kowaliszyn.zuzanna.tapptic.viewModel.fragment.base.BaseFragmentViewModel
import kowaliszyn.zuzanna.tapptic.viewModel.repository.fragment.DetailsFragmentRepository
import javax.inject.Inject

@HiltViewModel
class DetailsFragmentViewModel @Inject constructor(
    repository: DetailsFragmentRepository,
    savedStateHandle: SavedStateHandle
) : BaseFragmentViewModel<FragmentDetailsBinding, DetailsFragmentRepository>(
    repository,
    savedStateHandle
) {

    val name = ScopedLiveData("")
    val isWaiting = ScopedLiveData(false)
    val description = ScopedLiveData("")

    fun update(itemName: String) {
        name.value = itemName
        onScope {
            repository.getItemDetails(itemName)?.let { itemData ->
                onMain {
                    description.value = itemData.text
                    binding?.detailsDialogItemImage?.let { imageView ->
                        isWaiting.value = true
                        Picasso.get().loadInto(
                            itemData.image, imageView,
                            onSuccess = {
                                isWaiting.value = false
                            },
                            onError = {
                                isWaiting.value = false
                                imageView.background =
                                    ContextCompat.getDrawable(context, R.drawable.ic_logo)
                            }
                        )
                    }
                }
            }
        }
    }
}
