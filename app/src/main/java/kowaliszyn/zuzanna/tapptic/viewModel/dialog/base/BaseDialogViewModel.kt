package kowaliszyn.zuzanna.tapptic.viewModel.dialog.base

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.SavedStateHandle
import kowaliszyn.zuzanna.tapptic.utils.liveData.EventLiveData
import kowaliszyn.zuzanna.tapptic.viewModel.base.BaseViewModel
import kowaliszyn.zuzanna.tapptic.viewModel.repository.base.BaseRepository

abstract class BaseDialogViewModel<B : ViewDataBinding, out R : BaseRepository> constructor(
    repository: R,
    savedStateHandle: SavedStateHandle
) : BaseViewModel<B, R>(repository, savedStateHandle) {

    val closeDialogEvent = EventLiveData()
}
