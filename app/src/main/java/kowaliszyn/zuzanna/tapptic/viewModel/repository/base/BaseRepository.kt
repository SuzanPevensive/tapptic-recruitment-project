package kowaliszyn.zuzanna.tapptic.viewModel.repository.base

import android.content.Context
import kowaliszyn.zuzanna.tapptic.converter.DataConverter
import kowaliszyn.zuzanna.tapptic.manager.DataManager
import kowaliszyn.zuzanna.tapptic.manager.NetworkManager

abstract class BaseRepository constructor(
    val context: Context,
    val dataConverter: DataConverter,
    val dataManager: DataManager,
    val networkManager: NetworkManager
)
