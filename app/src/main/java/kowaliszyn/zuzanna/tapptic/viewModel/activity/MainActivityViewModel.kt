package kowaliszyn.zuzanna.tapptic.viewModel.activity

import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.SavedStateHandle
import dagger.hilt.android.lifecycle.HiltViewModel
import kowaliszyn.zuzanna.tapptic.const.ChunkListsConst
import kowaliszyn.zuzanna.tapptic.databinding.ActivityMainBinding
import kowaliszyn.zuzanna.tapptic.model.data.ItemData
import kowaliszyn.zuzanna.tapptic.sealeds.ItemState
import kowaliszyn.zuzanna.tapptic.ui.adapter.ListItemsAdapter
import kowaliszyn.zuzanna.tapptic.ui.layoutManager.StateLayoutManager
import kowaliszyn.zuzanna.tapptic.utils.extension.global.onMain
import kowaliszyn.zuzanna.tapptic.utils.extension.global.onScope
import kowaliszyn.zuzanna.tapptic.utils.extension.isLandscape
import kowaliszyn.zuzanna.tapptic.utils.liveData.ScopedLiveData
import kowaliszyn.zuzanna.tapptic.viewModel.base.BaseViewModel
import kowaliszyn.zuzanna.tapptic.viewModel.repository.activity.MainActivityRepository
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.timerTask

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    repository: MainActivityRepository,
    savedStateHandle: SavedStateHandle,
) : BaseViewModel<ActivityMainBinding, MainActivityRepository>(
    repository,
    savedStateHandle
) {

    val adapter = ListItemsAdapter(listOf()) { itemName ->
        selectItem(itemName)
        itemTapName.value = itemName
    }

    private var listSavedState: Parcelable? = null
    @VisibleForTesting
    var listSavedData: List<ItemData>? = null
    private var layoutManager: StateLayoutManager? = null

    private var listItemsSwipeRefreshTimestamp = 0L
    val isWaiting = ScopedLiveData(false)
    val itemTapName = ScopedLiveData<String>()

    val isFragmentContainerVisible = ScopedLiveData(false)

    fun getSelectedOrFirstItemName() =
        (itemTapName.value ?: adapter.getData().firstOrNull()?.name)?.also { selectItem(it) }

    fun selectItem(itemName: String) {
        adapter.itemState = ItemState.Selected(itemName)
    }
    fun unSelectItem(itemName: String) {
        adapter.itemState = ItemState.Focused(itemName)
    }

    override fun init(
        lifecycleOwner: LifecycleOwner?,
        layoutId: Int,
        inflater: LayoutInflater,
        container: ViewGroup?,
    ) {
        super.init(lifecycleOwner, layoutId, inflater, container)
        isFragmentContainerVisible.value = context.isLandscape()
        binding?.listItemsSwipeRefresh?.setOnRefreshListener(::refreshLayout)
        binding?.listItems?.let { recyclerView ->
            recyclerView.adapter = adapter.apply {
                listSavedData?.let { setData(it) }
            }
            layoutManager = StateLayoutManager(context).apply {
                recyclerView.layoutManager = this
                listSavedState?.let { onRestoreInstanceState(it) }
            }
        }
        if (listSavedData == null) loadListItemsNextChunk(refreshList = true)
    }

    override fun joinToLifeCycle(lifecycleOwner: LifecycleOwner) {
        super.joinToLifeCycle(lifecycleOwner)
        adapter.joinToLifeCycle(lifecycleOwner)
    }

    fun refreshLayout(showWaiting: Boolean = false) {
        loadListItemsNextChunk(refreshList = true, showWaiting = showWaiting)
    }

    override fun onSaveState() {
        listSavedState = layoutManager?.onSaveInstanceState()
        listSavedData = adapter.getData()
    }

    fun loadListItemsNextChunk(
        refreshList: Boolean = false,
        showWaiting: Boolean = true
    ) {
        listItemsSwipeRefreshTimestamp = System.currentTimeMillis()
        isWaiting.value = showWaiting
        layoutManager?.disable()
        onScope {
            if (refreshList) repository.updateListItems()
            repository.getListItemsNextChunk().let { items ->
                onMain {
                    if (refreshList) adapter.setData(items) else adapter.addData(items)
                    if (itemTapName.value.isNullOrEmpty() && context.isLandscape()) {
                        itemTapName.value = items.firstOrNull()?.name
                    }
                }
            }
            onMain {
                if (adapter.maxItemCount == 0) {
                    adapter.maxItemCount = repository.getListItemsSize()
                }
                layoutManager?.enable()
                hideLoaders()
            }
        }
    }

    // functionality only for keep alive loader for minimum millis
    // and prevention loader blinking
    private fun hideLoaders() {
        val timestampDiff = ChunkListsConst.LIST_CHUNK_LOAD_MIN_DELAY -
            (System.currentTimeMillis() - listItemsSwipeRefreshTimestamp)
        if (timestampDiff > 0) {
            Timer(false).schedule(
                timerTask {
                    isWaiting.value = false
                    binding?.listItemsSwipeRefresh?.isRefreshing = false
                },
                timestampDiff
            )
        } else {
            isWaiting.value = false
            binding?.listItemsSwipeRefresh?.isRefreshing = false
        }
    }
}
