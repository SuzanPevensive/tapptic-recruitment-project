package kowaliszyn.zuzanna.tapptic.viewModel.dialog

import androidx.lifecycle.SavedStateHandle
import dagger.hilt.android.lifecycle.HiltViewModel
import kowaliszyn.zuzanna.tapptic.databinding.InfoBarConnectionMissingBinding
import kowaliszyn.zuzanna.tapptic.utils.event.impl.SingleClickEvent
import kowaliszyn.zuzanna.tapptic.viewModel.dialog.base.BaseDialogViewModel
import kowaliszyn.zuzanna.tapptic.viewModel.repository.dialog.InfoBarConnectionMissingDialogRepository
import javax.inject.Inject

@HiltViewModel
class InfoBarConnectionMissingDialogViewModel @Inject constructor(
    repositoryConnectionMissing: InfoBarConnectionMissingDialogRepository,
    savedStateHandle: SavedStateHandle
) : BaseDialogViewModel<InfoBarConnectionMissingBinding, InfoBarConnectionMissingDialogRepository>(repositoryConnectionMissing, savedStateHandle) {

    fun tryReconnect(event: SingleClickEvent) {
        closeDialogEvent.run()
        event.done()
    }
}
