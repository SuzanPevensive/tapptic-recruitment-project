package kowaliszyn.zuzanna.tapptic.sealeds

sealed class ItemState constructor(val name: String) {
    class Normal(name: String) : ItemState(name)
    class Focused(name: String) : ItemState(name)
    class Selected(name: String) : ItemState(name)
}
