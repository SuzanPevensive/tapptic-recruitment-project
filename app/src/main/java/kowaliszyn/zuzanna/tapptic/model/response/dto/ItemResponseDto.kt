package kowaliszyn.zuzanna.tapptic.model.response.dto

import kowaliszyn.zuzanna.tapptic.model.response.dto.base.BaseResponseDto

data class ItemResponseDto(
    val name: String?,
    val text: String?,
    val image: String?,
) : BaseResponseDto
