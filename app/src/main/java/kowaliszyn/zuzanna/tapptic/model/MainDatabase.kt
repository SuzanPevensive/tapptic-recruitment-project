package kowaliszyn.zuzanna.tapptic.model

import androidx.room.Database
import androidx.room.RoomDatabase
import kowaliszyn.zuzanna.tapptic.model.dao.ItemDataDao
import kowaliszyn.zuzanna.tapptic.model.data.ItemData

@Database(entities = [ItemData::class], version = 1, exportSchema = false)
abstract class MainDatabase : RoomDatabase() {
    abstract fun itemsEntityDao(): ItemDataDao
}
