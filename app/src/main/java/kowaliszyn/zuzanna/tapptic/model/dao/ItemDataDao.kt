package kowaliszyn.zuzanna.tapptic.model.dao

import androidx.room.*
import kowaliszyn.zuzanna.tapptic.model.data.ItemData

@Dao
interface ItemDataDao {

    @Query("SELECT * FROM Items")
    suspend fun getAllItems(): List<ItemData>

    @Query("SELECT * FROM Items LIMIT :count OFFSET :offset")
    suspend fun getSomeItems(offset: Int, count: Int): List<ItemData>

    @Query("SELECT * FROM Items WHERE name LIKE :name LIMIT 1")
    suspend fun findItemById(name: String): ItemData

    @Insert
    suspend fun addItem(item: ItemData)

    @Insert
    suspend fun addItems(vararg items: ItemData)

    @Update
    suspend fun editItem(vararg item: ItemData)

    @Delete
    suspend fun removeItem(item: ItemData)

    @Query("DELETE FROM Items")
    fun removeAllItems()
}
