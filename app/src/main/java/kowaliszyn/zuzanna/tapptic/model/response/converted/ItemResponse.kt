package kowaliszyn.zuzanna.tapptic.model.response.converted

import kowaliszyn.zuzanna.tapptic.model.response.converted.base.BaseResponse

data class ItemResponse(
    val name: String,
    val text: String,
    val image: String,
) : BaseResponse
