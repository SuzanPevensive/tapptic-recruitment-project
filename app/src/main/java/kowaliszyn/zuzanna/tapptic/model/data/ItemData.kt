package kowaliszyn.zuzanna.tapptic.model.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import kowaliszyn.zuzanna.tapptic.model.data.base.BaseData

@Entity(tableName = "Items")
data class ItemData(
    @PrimaryKey var name: String,
    var text: String,
    var image: String,
) : BaseData
