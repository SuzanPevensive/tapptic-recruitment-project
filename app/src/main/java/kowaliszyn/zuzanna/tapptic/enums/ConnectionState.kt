package kowaliszyn.zuzanna.tapptic.enums

enum class ConnectionState {
    CONNECTED, NOT_CONNECTED
}
