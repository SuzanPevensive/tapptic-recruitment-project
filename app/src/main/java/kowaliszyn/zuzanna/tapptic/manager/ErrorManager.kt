package kowaliszyn.zuzanna.tapptic.manager

import androidx.lifecycle.LifecycleOwner

interface ErrorManager {

    fun handleErrors(lifecycleOwner: LifecycleOwner, action: (Throwable) -> Unit)

    fun getAll(type: Class<out Throwable>? = null): List<Throwable>
    fun add(error: Throwable)
    fun get(type: Class<out Throwable>, index: Int? = null): Throwable?
    fun get(index: Int? = null): Throwable?
    fun remove(type: Class<out Throwable>? = null, index: Int? = null)
    fun clear()
}
