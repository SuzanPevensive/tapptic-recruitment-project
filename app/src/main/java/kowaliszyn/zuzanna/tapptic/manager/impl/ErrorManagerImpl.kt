package kowaliszyn.zuzanna.tapptic.manager.impl

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LifecycleOwner
import javax.inject.Inject
import kowaliszyn.zuzanna.tapptic.manager.ErrorManager
import kowaliszyn.zuzanna.tapptic.utils.liveData.ScopedLiveData

class ErrorManagerImpl @Inject constructor() : ErrorManager {

    @VisibleForTesting
    val lastError = ScopedLiveData<Throwable>()
    private val allErrors = mutableMapOf<Class<out Throwable>, MutableList<Throwable>>()

    override fun handleErrors(lifecycleOwner: LifecycleOwner, action: (Throwable) -> Unit) {
        var actualError: Throwable? = lastError.value
        lastError.observe(lifecycleOwner) {
            if (actualError != it) action(it)
            actualError = null
        }
    }

    override fun getAll(type: Class<out Throwable>?): List<Throwable> {
        return if (type == null) allErrors.flatMap { it.value } else allErrors[type] ?: listOf()
    }

    override fun add(error: Throwable) {
        allErrors[error::class.java] = (allErrors[error::class.java] ?: mutableListOf()).apply {
            add(error)
        }
        lastError.value = error
        error.printStackTrace()
    }

    override fun get(index: Int?) = getAll().run {
        if (index == null) lastOrNull() else getOrNull(index)
    }
    override fun get(type: Class<out Throwable>, index: Int?) =
        allErrors[type]?.run { if (index == null) lastOrNull() else getOrNull(index) }

    override fun remove(type: Class<out Throwable>?, index: Int?) {
        when {
            type == null -> allErrors.clear()
            index == null -> allErrors[type]?.clear()
            else -> allErrors[type]?.removeAt(index)
        }
    }

    override fun clear() = remove()
}
