package kowaliszyn.zuzanna.tapptic.manager

import kowaliszyn.zuzanna.tapptic.model.data.ItemData

interface DataManager {

    suspend fun clearAllData()

    suspend fun getAllItems(): List<ItemData>
    suspend fun getItems(chunk: Int, chunkNumber: Int = 1): List<ItemData>
    suspend fun getItem(itemName: String): ItemData?
    suspend fun addItems(items: List<ItemData>)
    suspend fun addItem(item: ItemData)
    suspend fun editItem(item: ItemData)
    suspend fun removeItem(name: String)
    suspend fun removeAllItems()
}
