package kowaliszyn.zuzanna.tapptic.manager.impl

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kowaliszyn.zuzanna.tapptic.converter.ResponseConverter
import kowaliszyn.zuzanna.tapptic.manager.ErrorManager
import kowaliszyn.zuzanna.tapptic.manager.NetworkManager
import kowaliszyn.zuzanna.tapptic.network.Endpoints
import kowaliszyn.zuzanna.tapptic.utils.extension.global.withScope
import javax.inject.Inject

class NetworkManagerImpl @Inject constructor(
    @ApplicationContext val context: Context,
    private val endpoints: Endpoints,
    private val errorManager: ErrorManager,
    private val responseConverter: ResponseConverter,
) : NetworkManager {

    private val defaultDispatcher = Dispatchers.Default

    private suspend fun <R> runOnNetworkScope(action: () -> Deferred<R>) =
        withContext(defaultDispatcher) {
            try {
                action().await()
            } catch (e: Throwable) {
                errorManager.add(e)
                null
            }
        }

    override suspend fun isConnected() =
        withScope {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val network = connectivityManager.activeNetwork ?: return@withScope false
                val networkCapabilities =
                    connectivityManager.getNetworkCapabilities(network) ?: return@withScope false
                when {
                    networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                        networkCapabilities.hasTransport(
                            NetworkCapabilities.TRANSPORT_CELLULAR
                        ) ||
                        networkCapabilities.hasTransport(
                            NetworkCapabilities.TRANSPORT_ETHERNET
                        ) -> true
                    else -> false
                }
            } else {
                @Suppress("DEPRECATION")
                connectivityManager.activeNetworkInfo?.run {
                    when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }
                } ?: false
            }
        }

    override suspend fun getListItems() =
        runOnNetworkScope {
            endpoints.getListItemsAsync()
        }?.let(responseConverter::convertListItemsResponse)

    override suspend fun getItemsDetails(itemName: String) =
        runOnNetworkScope {
            endpoints.getItemAsync(itemName)
        }?.let(responseConverter::convertItemResponse)
}
