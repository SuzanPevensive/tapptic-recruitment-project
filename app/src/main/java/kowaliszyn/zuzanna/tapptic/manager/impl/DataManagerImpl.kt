package kowaliszyn.zuzanna.tapptic.manager.impl

import android.content.Context
import androidx.room.Room
import dagger.hilt.android.qualifiers.ApplicationContext
import kowaliszyn.zuzanna.tapptic.const.ChunkListsConst
import kowaliszyn.zuzanna.tapptic.const.DatabaseConst
import kowaliszyn.zuzanna.tapptic.manager.DataManager
import kowaliszyn.zuzanna.tapptic.model.MainDatabase
import kowaliszyn.zuzanna.tapptic.model.data.ItemData
import javax.inject.Inject

class DataManagerImpl @Inject constructor(
    @ApplicationContext private val context: Context
) : DataManager {

    private val mainDatabase = Room.databaseBuilder(
        context,
        MainDatabase::class.java,
        DatabaseConst.MAIN_DATABASE_NAME
    ).run {
        fallbackToDestructiveMigration()
        build()
    }

    private val itemsBase = mainDatabase.itemsEntityDao()

    override suspend fun clearAllData() {
        itemsBase.removeAllItems()
    }

    override suspend fun getAllItems() = itemsBase.getAllItems()

    override suspend fun getItems(chunk: Int, chunkNumber: Int) = itemsBase.getSomeItems(
        ChunkListsConst.LIST_CHUNK_SIZE * chunk,
        ChunkListsConst.LIST_CHUNK_SIZE * chunkNumber
    )

    override suspend fun getItem(itemName: String) = itemsBase.findItemById(itemName)

    override suspend fun addItems(items: List<ItemData>) =
        itemsBase.addItems(*items.toTypedArray())

    override suspend fun addItem(item: ItemData) = itemsBase.addItem(item)

    override suspend fun editItem(item: ItemData) = itemsBase.editItem(item)

    override suspend fun removeItem(name: String) = itemsBase.removeItem(
        ItemData(name, "", "")
    )

    override suspend fun removeAllItems() = itemsBase.removeAllItems()
}
