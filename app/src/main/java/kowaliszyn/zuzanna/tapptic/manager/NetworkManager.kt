package kowaliszyn.zuzanna.tapptic.manager

import kowaliszyn.zuzanna.tapptic.model.response.converted.*

interface NetworkManager {

    suspend fun isConnected(): Boolean

    suspend fun getListItems(): List<ItemResponse>?
    suspend fun getItemsDetails(itemName: String): ItemResponse?
}
