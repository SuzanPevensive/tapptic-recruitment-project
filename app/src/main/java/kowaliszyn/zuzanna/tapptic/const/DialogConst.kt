package kowaliszyn.zuzanna.tapptic.const

object DialogConst {

    const val TAG_DIALOG_DETAILS = "DIALOG_DETAILS"
    const val TAG_DIALOG_CONNECTION_MISSING = "DIALOG_CONNECTION_MISSING"
}
