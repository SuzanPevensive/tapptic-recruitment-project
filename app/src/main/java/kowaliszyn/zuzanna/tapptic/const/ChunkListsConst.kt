package kowaliszyn.zuzanna.tapptic.const

object ChunkListsConst {
    const val LiST_OFFSET_ITEM_TO_NEXT_PAGE = 2
    const val LIST_CHUNK_LOAD_MIN_DELAY = 500L
    const val LIST_CHUNK_SIZE = 10
}
