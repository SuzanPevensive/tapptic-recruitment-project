package kowaliszyn.zuzanna.tapptic.const

import java.util.Locale

object LocaleConst {
    val DEFAULT_LOCALE: Locale get() = Locale.getDefault()
}
