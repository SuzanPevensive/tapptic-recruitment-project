package kowaliszyn.zuzanna.tapptic.const

object FragmentConst {

    const val TAG_FRAGMENT_DETAILS = "FRAGMENT_DETAILS"

    const val ARGUMENT_ITEM_NAME = "ITEM_NAME"
}
