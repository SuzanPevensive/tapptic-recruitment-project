package kowaliszyn.zuzanna.tapptic.worker

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kowaliszyn.zuzanna.tapptic.manager.NetworkManager

@HiltWorker
class CheckConnectionWorker @AssistedInject constructor(
    @Assisted context: Context,
    @Assisted workerParams: WorkerParameters,
    private val networkManager: NetworkManager
) : CoroutineWorker(context, workerParams) {

    override suspend fun doWork() =
        if (networkManager.isConnected()) Result.success() else Result.failure()
}
