package kowaliszyn.zuzanna.tapptic.ui.activity

import android.os.Bundle
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.tapptic.R
import kowaliszyn.zuzanna.tapptic.const.DialogConst
import kowaliszyn.zuzanna.tapptic.const.FragmentConst
import kowaliszyn.zuzanna.tapptic.enums.ConnectionState
import kowaliszyn.zuzanna.tapptic.manager.ErrorManager
import kowaliszyn.zuzanna.tapptic.ui.activity.base.BaseActivity
import kowaliszyn.zuzanna.tapptic.ui.dialog.DetailsDialog
import kowaliszyn.zuzanna.tapptic.ui.fragment.DetailsFragment
import kowaliszyn.zuzanna.tapptic.utils.extension.isLandscape
import kowaliszyn.zuzanna.tapptic.viewModel.activity.MainActivityViewModel
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity<MainActivityViewModel>() {

    override val viewModel by viewModels<MainActivityViewModel>()

    @Inject
    override lateinit var errorManager: ErrorManager
    override val layoutId = R.layout.activity_main

    private var detailsFragment: DetailsFragment? = null
    private var detailsDialog: DetailsDialog? = null

    private fun attachFragment() {
        detailsFragment = DetailsFragment.newInstance().also { detailsFragment ->
            viewModel.getSelectedOrFirstItemName()?.let { itemName ->
                detailsFragment.changeItemDetails(itemName)
            }
            supportFragmentManager.beginTransaction().replace(
                R.id.main_fragment_container,
                detailsFragment,
                FragmentConst.TAG_FRAGMENT_DETAILS
            ).apply { commit() }
        }
    }

    private fun createDialog() {
        detailsDialog = DetailsDialog.newInstance {
            viewModel.itemTapName.value?.let(viewModel::unSelectItem)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        detailsFragment = null
        detailsDialog = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (isLandscape()) attachFragment()
        else createDialog()
    }

    override fun setObservers() {
        connectionState.observe(this) { connectionState ->
            if (connectionState == ConnectionState.CONNECTED) {
                viewModel.refreshLayout(showWaiting = true)
            }
        }
        viewModel.itemTapName.observe(this) { itemName ->
            if (!itemName.isNullOrEmpty()) {
                detailsDialog?.apply {
                    changeItemDetails(itemName)
                    show(supportFragmentManager, DialogConst.TAG_DIALOG_DETAILS)
                }
                detailsFragment?.changeItemDetails(itemName)
            }
        }
        viewModel.adapter.loadingNextChunk.observe(this) {
            if (it) viewModel.loadListItemsNextChunk()
        }
    }
}
