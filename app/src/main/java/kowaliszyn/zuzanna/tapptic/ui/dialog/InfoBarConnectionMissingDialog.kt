package kowaliszyn.zuzanna.tapptic.ui.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.tapptic.R
import kowaliszyn.zuzanna.tapptic.ui.dialog.base.BaseDialog
import kowaliszyn.zuzanna.tapptic.utils.extension.isLandscape
import kowaliszyn.zuzanna.tapptic.utils.extension.windowSize
import kowaliszyn.zuzanna.tapptic.viewModel.dialog.InfoBarConnectionMissingDialogViewModel

@AndroidEntryPoint
class InfoBarConnectionMissingDialog : BaseDialog<InfoBarConnectionMissingDialogViewModel>() {

    companion object {
        fun newInstance(
            closeAction: (() -> Unit)? = null
        ) = InfoBarConnectionMissingDialog().apply {
            this.closeAction = closeAction
        }
    }

    override var closeAction: (() -> Unit)? = null

    override fun onDestroy() {
        super.onDestroy()
        closeAction?.invoke()
    }

    override val viewModel by viewModels<InfoBarConnectionMissingDialogViewModel>()
    override val layoutId = R.layout.info_bar_connection_missing

    override fun onDrawWindow(window: Window) {
        window.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            attributes = attributes.apply {
                horizontalMargin = 0F
                verticalMargin = 0F
                activity?.windowSize?.let { size ->
                    height = if (context.isLandscape()) size.height - 48 // info bar height
                    else size.height
                    width = size.width
                }
            }
        }
    }
}
