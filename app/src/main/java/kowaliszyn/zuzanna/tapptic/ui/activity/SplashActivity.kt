package kowaliszyn.zuzanna.tapptic.ui.activity

import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.tapptic.R
import kowaliszyn.zuzanna.tapptic.const.ActivityConst
import kowaliszyn.zuzanna.tapptic.manager.ErrorManager
import kowaliszyn.zuzanna.tapptic.ui.activity.base.BaseActivity
import kowaliszyn.zuzanna.tapptic.utils.extension.goTo
import kowaliszyn.zuzanna.tapptic.utils.extension.uiTimer
import kowaliszyn.zuzanna.tapptic.viewModel.activity.SplashActivityViewModel
import javax.inject.Inject

@AndroidEntryPoint
class SplashActivity : BaseActivity<SplashActivityViewModel>() {

    override val viewModel by viewModels<SplashActivityViewModel>()

    @Inject
    override lateinit var errorManager: ErrorManager
    override val layoutId = R.layout.activity_splash

    override val checkConnectionState = false

    override fun setObservers() {
        viewModel.goToMainActivityEvent.observe(this) {
            uiTimer(ActivityConst.DELAY_SPLASH_SCREEN) {
                goTo<MainActivity>()
            }
        }
    }
}
