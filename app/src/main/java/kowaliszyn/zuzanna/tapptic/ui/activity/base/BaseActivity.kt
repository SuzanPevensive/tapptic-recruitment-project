package kowaliszyn.zuzanna.tapptic.ui.activity.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import kowaliszyn.zuzanna.tapptic.const.DialogConst
import kowaliszyn.zuzanna.tapptic.const.WorkConst
import kowaliszyn.zuzanna.tapptic.enums.ConnectionState
import kowaliszyn.zuzanna.tapptic.manager.ErrorManager
import kowaliszyn.zuzanna.tapptic.ui.dialog.InfoBarConnectionMissingDialog
import kowaliszyn.zuzanna.tapptic.utils.extension.uiTimer
import kowaliszyn.zuzanna.tapptic.utils.liveData.UniqueLiveData
import kowaliszyn.zuzanna.tapptic.viewModel.base.BaseViewModel
import kowaliszyn.zuzanna.tapptic.worker.CheckConnectionWorker

abstract class BaseActivity<out VM : BaseViewModel<*, *>> : AppCompatActivity() {

    abstract val viewModel: VM

    protected abstract val errorManager: ErrorManager
    protected abstract val layoutId: Int

    protected open val checkConnectionState = true
    val connectionState = UniqueLiveData(ConnectionState.CONNECTED)

    private val workerManager by lazy { WorkManager.getInstance(applicationContext) }
    private val checkConnectionWorkRequest =
        OneTimeWorkRequestBuilder<CheckConnectionWorker>().build()

    private val connectionMissingDialog by lazy {
        InfoBarConnectionMissingDialog.newInstance {
            runCheckConnectionWorkRequest()
        }
    }

    private fun showConnectionMissingDialog() {
        if (!connectionMissingDialog.isVisible) {
            connectionMissingDialog.show(
                supportFragmentManager,
                DialogConst.TAG_DIALOG_CONNECTION_MISSING
            )
        }
    }

    private fun hideConnectionMissingDialog() {
        if (connectionMissingDialog.isVisible) {
            connectionMissingDialog.dismiss()
        }
    }

    private fun runCheckConnectionWorkRequest() {
        uiTimer(500) {
            workerManager.enqueueUniqueWork(
                WorkConst.WORK_NAME_CHECK_CONNECTION,
                ExistingWorkPolicy.REPLACE,
                checkConnectionWorkRequest
            )
        }
    }

    override fun onSaveInstanceState(state: Bundle) {
        super.onSaveInstanceState(state)
        viewModel.onSaveState()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.init(this, layoutId, LayoutInflater.from(this))
        setContentView(viewModel.rootView)

        setObservers()
        errorManager.handleErrors(this, viewModel::handleError)

        workerManager
            .getWorkInfoByIdLiveData(checkConnectionWorkRequest.id)
            .observe(
                this,
                { info ->
                    when {
                        info == null -> runCheckConnectionWorkRequest()
                        info.state == WorkInfo.State.SUCCEEDED -> {
                            connectionState.value = ConnectionState.CONNECTED
                            hideConnectionMissingDialog()
                            runCheckConnectionWorkRequest()
                        }
                        info.state == WorkInfo.State.FAILED -> {
                            connectionState.value = ConnectionState.NOT_CONNECTED
                            showConnectionMissingDialog()
                        }
                        info.state == WorkInfo.State.BLOCKED ||
                            info.state == WorkInfo.State.CANCELLED -> {
                            runCheckConnectionWorkRequest()
                        }
                    }
                }
            )
        if (checkConnectionState) runCheckConnectionWorkRequest()
    }

    open fun setObservers() {
    }

    override fun onBackPressed() {
    }
}
