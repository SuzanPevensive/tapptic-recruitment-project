package kowaliszyn.zuzanna.tapptic.ui.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.tapptic.R
import kowaliszyn.zuzanna.tapptic.const.FragmentConst
import kowaliszyn.zuzanna.tapptic.ui.dialog.base.BaseDialog
import kowaliszyn.zuzanna.tapptic.ui.fragment.DetailsFragment
import kowaliszyn.zuzanna.tapptic.utils.extension.isLandscape
import kowaliszyn.zuzanna.tapptic.viewModel.dialog.DetailsDialogViewModel

@AndroidEntryPoint
class DetailsDialog : BaseDialog<DetailsDialogViewModel>() {

    companion object {
        fun newInstance(closeAction: (() -> Unit)) =
            DetailsDialog().apply {
                this.closeAction = closeAction
            }
    }

    override val viewModel by viewModels<DetailsDialogViewModel>()
    override val layoutId = R.layout.dialog_details
    override var closeAction: (() -> Unit)? = null

    private val detailsFragment = DetailsFragment.newInstance()

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        closeAction?.invoke()
    }

    fun changeItemDetails(itemName: String) {
        detailsFragment.arguments?.putString(FragmentConst.ARGUMENT_ITEM_NAME, itemName)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = true
        if (context?.isLandscape() == true) dismiss()
        else {
            childFragmentManager.beginTransaction()
                .add(
                    R.id.details_dialog_fragment_container,
                    detailsFragment,
                    FragmentConst.TAG_FRAGMENT_DETAILS
                ).commit()
        }
    }
}
