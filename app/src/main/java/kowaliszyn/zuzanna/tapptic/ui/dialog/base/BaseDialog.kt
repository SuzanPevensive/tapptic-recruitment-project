package kowaliszyn.zuzanna.tapptic.ui.dialog.base

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import kowaliszyn.zuzanna.tapptic.utils.extension.windowSize
import kowaliszyn.zuzanna.tapptic.viewModel.dialog.base.BaseDialogViewModel

abstract class BaseDialog<out VM : BaseDialogViewModel<*, *>> : DialogFragment() {

    abstract val viewModel: VM
    protected abstract val layoutId: Int
    protected abstract val closeAction: (() -> Unit)?

    override fun onResume() {
        super.onResume()
        dialog?.window?.let(::onDrawWindow)
    }

    protected open fun onDrawWindow(window: Window) {
        window.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            attributes = attributes.apply {
                horizontalMargin = 0F
                verticalMargin = 0F
                activity?.windowSize?.let { size ->
                    width = size.width
                }
            }
        }
    }

    protected open fun setObservers() {
        viewModel.closeDialogEvent.observe(viewLifecycleOwner) {
            dismiss()
            closeAction?.invoke()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.init(null, layoutId, LayoutInflater.from(context))
        return viewModel.rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.joinToLifeCycle(viewLifecycleOwner)
        isCancelable = false

        setObservers()
    }
}
