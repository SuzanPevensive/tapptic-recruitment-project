package kowaliszyn.zuzanna.tapptic.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.tapptic.R
import kowaliszyn.zuzanna.tapptic.const.FragmentConst
import kowaliszyn.zuzanna.tapptic.manager.ErrorManager
import kowaliszyn.zuzanna.tapptic.ui.fragment.base.BaseFragment
import kowaliszyn.zuzanna.tapptic.viewModel.fragment.DetailsFragmentViewModel
import javax.inject.Inject

@AndroidEntryPoint
class DetailsFragment : BaseFragment<Boolean, DetailsFragmentViewModel>() {

    companion object {
        fun newInstance() = DetailsFragment().apply {
            arguments = bundleOf()
        }
    }

    @Inject
    override lateinit var errorManager: ErrorManager
    override val layoutId = R.layout.fragment_details

    override val viewModel by viewModels<DetailsFragmentViewModel>()

    fun changeItemDetails(itemName: String) {
        if (isAdded) viewModel.update(itemName)
        else arguments?.putString(FragmentConst.ARGUMENT_ITEM_NAME, itemName)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString(FragmentConst.ARGUMENT_ITEM_NAME)?.let(viewModel::update)
    }
}
