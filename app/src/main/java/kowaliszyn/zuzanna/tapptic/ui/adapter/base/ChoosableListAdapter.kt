package kowaliszyn.zuzanna.tapptic.ui.adapter.base

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import kowaliszyn.zuzanna.tapptic.const.ChunkListsConst
import kowaliszyn.zuzanna.tapptic.utils.event.impl.SingleClickEvent

abstract class ChoosableListAdapter
<T, VM : ChoosableListAdapter.ChoosableViewHolderModel<T, B>, B : ViewDataBinding>(
    list: List<T>,
    lifecycleOwner: LifecycleOwner? = null,
    layoutId: Int,
    offsetToLoadChunk: Int = ChunkListsConst.LiST_OFFSET_ITEM_TO_NEXT_PAGE
) : SimpleListAdapter<T, VM, B>(list, lifecycleOwner, layoutId, offsetToLoadChunk) {

    abstract class ChoosableViewHolderModel<T, B : ViewDataBinding>(
        binding: B,
        private val chooseItemAction: (Int) -> Unit
    ) : SimpleViewHolderModel<T, B>(binding) {

        open fun chooseItem(event: SingleClickEvent) {
            chooseItemAction(position)
            event.done()
        }

        abstract override fun setData(data: T)
    }
}
