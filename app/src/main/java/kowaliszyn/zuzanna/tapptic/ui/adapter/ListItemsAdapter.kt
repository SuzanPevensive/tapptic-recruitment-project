package kowaliszyn.zuzanna.tapptic.ui.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.squareup.picasso.Picasso
import kowaliszyn.zuzanna.tapptic.R
import kowaliszyn.zuzanna.tapptic.databinding.ItemListBinding
import kowaliszyn.zuzanna.tapptic.model.data.ItemData
import kowaliszyn.zuzanna.tapptic.sealeds.ItemState
import kowaliszyn.zuzanna.tapptic.ui.adapter.base.ChoosableListAdapter
import kowaliszyn.zuzanna.tapptic.utils.extension.loadInto
import kowaliszyn.zuzanna.tapptic.utils.liveData.ScopedLiveData
import kowaliszyn.zuzanna.tapptic.utils.liveData.UniqueLiveData

class ListItemsAdapter constructor(
    list: List<ItemData>,
    lifecycleOwner: LifecycleOwner? = null,
    private val chooseItemAction: (name: String) -> Unit,
) : ChoosableListAdapter
<ItemData, ListItemsAdapter.ListItemsViewHolderModel, ItemListBinding>(
    list,
    lifecycleOwner,
    R.layout.item_list
) {

    var itemState: ItemState? = null
        set(v) {
            list.indexOfFirst { it.name == v?.name }.let { index ->
                if (index >= 0) queueItemChanged(index)
            }
            list.indexOfFirst { it.name == field?.name }.let { index ->
                if (index >= 0) queueItemChanged(index)
            }
            field = v
        }

    override fun viewHolderModelBuilder(binding: ItemListBinding) =
        ListItemsViewHolderModel(binding) { position -> chooseItemAction(list[position].name) }

    inner class ListItemsViewHolderModel(
        binding: ItemListBinding,
        chooseItemAction: (Int) -> Unit
    ) : ChoosableListAdapter.ChoosableViewHolderModel<ItemData, ItemListBinding>(
        binding,
        chooseItemAction
    ) {

        private val context: Context = binding.root.context

        val name = UniqueLiveData("")
        val isWaiting = ScopedLiveData(false)

        private var imageUrl = ""

        val background = ScopedLiveData<Drawable>()

        override fun setData(data: ItemData) {
            name.value = data.name
            background.value = ColorDrawable(
                if (data.name == itemState?.name) {
                    when (itemState) {
                        is ItemState.Focused ->
                            ContextCompat.getColor(context, R.color.colorSecondaryDark)
                        is ItemState.Selected ->
                            ContextCompat.getColor(context, R.color.colorAccent)
                        else -> Color.TRANSPARENT
                    }
                } else Color.TRANSPARENT
            )
            if (imageUrl != data.image) {
                imageUrl = data.image
                isWaiting.value = true
                Picasso.get().loadInto(
                    imageUrl, binding.listItemImage,
                    onSuccess = {
                        isWaiting.value = false
                    },
                    onError = {
                        isWaiting.value = false
                        binding.listItemImage.background =
                            ContextCompat.getDrawable(context, R.drawable.ic_logo)
                    }
                )
            }
        }
    }
}
