package kowaliszyn.zuzanna.tapptic.ui.layoutManager

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler
import kowaliszyn.zuzanna.tapptic.utils.extension.global.onMainScope
import kowaliszyn.zuzanna.tapptic.utils.liveData.ScopedLiveData

class StateLayoutManager(context: Context) : LinearLayoutManager(context) {

    enum class State {
        ENABLED, DISABLED
    }

    private val state = ScopedLiveData(State.ENABLED)

    fun enable() {
        state.value = State.ENABLED
    }

    fun disable() {
        state.value = State.DISABLED
    }

    override fun canScrollVertically(): Boolean {
        return state.value == State.ENABLED && super.canScrollVertically()
    }

    override fun onLayoutChildren(recycler: Recycler?, state: RecyclerView.State?) {
        try {
            super.onLayoutChildren(recycler, state)
        } catch (e: Exception) {
            onMainScope {
                onLayoutChildren(recycler, state)
            }
        }
    }
}
