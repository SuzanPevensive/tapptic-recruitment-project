package kowaliszyn.zuzanna.tapptic.converter.base

import kowaliszyn.zuzanna.tapptic.const.CalendarConst
import java.util.*

open class BaseConverter {
    protected fun <T, R> convertList(list: List<T>?, converterMethod: (T) -> R): List<R> {
        return list?.map { item ->
            converterMethod(item)
        } ?: listOf()
    }

    protected fun convertDate(dateString: String?) = dateString.run {
        when {
            isNullOrEmpty() -> Date(0L)
            contains("Z") -> CalendarConst.DATETIME_Z_FORMAT.parse(this)
            contains("T") -> CalendarConst.DATETIME_T_FORMAT.parse(this)
            else -> CalendarConst.DATETIME_FORMAT.parse(this)
        } ?: Date(0L)
    }
}
