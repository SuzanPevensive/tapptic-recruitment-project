package kowaliszyn.zuzanna.tapptic.converter

import kowaliszyn.zuzanna.tapptic.const.DefaultsConst.defIfNull
import kowaliszyn.zuzanna.tapptic.converter.base.BaseConverter
import kowaliszyn.zuzanna.tapptic.model.response.converted.*
import kowaliszyn.zuzanna.tapptic.model.response.dto.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResponseConverter @Inject constructor(val dataConverter: DataConverter) : BaseConverter() {

    fun convertItemResponse(itemResponse: ItemResponseDto?) =
        ItemResponse(
            defIfNull(itemResponse?.name),
            defIfNull(itemResponse?.text),
            defIfNull(itemResponse?.image?.replace("http://", "https://"))
        )

    fun convertListItemsResponse(listItemsResponse: List<ItemResponseDto?>?) =
        convertList(listItemsResponse, ::convertItemResponse)
}
