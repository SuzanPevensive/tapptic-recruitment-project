package kowaliszyn.zuzanna.tapptic.converter

import kowaliszyn.zuzanna.tapptic.converter.base.BaseConverter
import kowaliszyn.zuzanna.tapptic.model.data.ItemData
import kowaliszyn.zuzanna.tapptic.model.response.converted.ItemResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataConverter @Inject constructor() : BaseConverter() {

    fun convertItemResponse(itemResponse: ItemResponse) =
        ItemData(
            itemResponse.name,
            itemResponse.text,
            itemResponse.image,
        )

    fun convertListItemsResponse(listItemsResponse: List<ItemResponse>) =
        convertList(listItemsResponse, ::convertItemResponse)
}
