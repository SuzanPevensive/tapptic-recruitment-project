package kowaliszyn.zuzanna.tapptic.utils.liveData

class EventLiveData : ClearableLiveData<Unit?>() {

    fun run() {
        value = Unit
    }
}
