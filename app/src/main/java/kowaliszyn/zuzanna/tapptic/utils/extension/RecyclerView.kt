package kowaliszyn.zuzanna.tapptic.utils.extension

import androidx.recyclerview.widget.RecyclerView
import kowaliszyn.zuzanna.tapptic.ui.layoutManager.StateLayoutManager

val RecyclerView.stateLayoutManager get() = layoutManager as StateLayoutManager
