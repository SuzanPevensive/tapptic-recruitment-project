package kowaliszyn.zuzanna.tapptic.utils.extension

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.util.DisplayMetrics
import android.util.Size
import kowaliszyn.zuzanna.tapptic.MainApplication
import java.util.Timer
import kotlin.concurrent.timerTask

fun Activity.uiTimer(delay: Long, action: () -> Unit) = this.uiTimer("", delay, action)
fun Activity.uiTimer(name: String, delay: Long, action: () -> Unit) {
    val timer = if (name.isEmpty()) Timer(false) else Timer(name, false)
    timer.schedule(
        timerTask {
            runOnUiThread {
                action.invoke()
            }
        },
        delay
    )
}

val Activity.windowSize: Size get() =
    DisplayMetrics().let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            app<MainApplication>().display?.getRealMetrics(it)
        } else {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(it)
        }
        return Size(it.widthPixels, it.heightPixels)
    }

inline fun <reified A : Activity> Activity.goTo() {
    Intent(this, A::class.java).let {
        startActivity(it)
        finish()
    }
}
