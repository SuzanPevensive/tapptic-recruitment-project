package kowaliszyn.zuzanna.tapptic.utils.extension

import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception

fun Picasso.loadInto(
    url: String,
    imageView: ImageView,
    onSuccess: () -> Unit,
    onError: (e: Exception?) -> Unit
) {
    this.load(url).apply {
        into(
            imageView,
            object : Callback {
                override fun onSuccess() = onSuccess()
                override fun onError(e: Exception?) = onError(e)
            }
        )
    }
}
