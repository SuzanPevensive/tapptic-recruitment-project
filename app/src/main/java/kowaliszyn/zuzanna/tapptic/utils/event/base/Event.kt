package kowaliszyn.zuzanna.tapptic.utils.event.base

interface Event {
    fun done()
}
