package kowaliszyn.zuzanna.tapptic.utils.extension

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast

fun Context.isLandscape() =
    resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE

fun Context.showToast(resId: Int, duration: Int = Toast.LENGTH_LONG) = showToast(
    getString(resId),
    duration
)
fun Context.showToast(message: String, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, message, duration).show()
}
fun Context.hideKeyboard(view: View) {
    val manager =
        getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
    manager.hideSoftInputFromWindow(view.windowToken, 0)
    view.clearFocus()
}
inline fun <reified T : Application> Context.app() = applicationContext as T
