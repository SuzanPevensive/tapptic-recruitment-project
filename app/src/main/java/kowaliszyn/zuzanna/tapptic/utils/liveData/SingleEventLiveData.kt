package kowaliszyn.zuzanna.tapptic.utils.liveData

class SingleEventLiveData : UniqueLiveData<Unit>() {

    fun run() {
        value = Unit
    }
}
