package kowaliszyn.zuzanna.tapptic.utils.extension

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.runBlocking

fun onTestScope(block: suspend CoroutineScope.() -> Unit) =
    runBlocking(block = block)
