package kowaliszyn.zuzanna.tapptic.test.utils.liveData

import android.graphics.Rect
import com.nhaarman.mockitokotlin2.spy
import kowaliszyn.zuzanna.tapptic.jUnitExtensions.InstantExecutorExtension
import kowaliszyn.zuzanna.tapptic.utils.liveData.UniqueLiveData
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class)
class UniqueLiveDataUnitTest {

    private var changes = 0

    private val startCollectionValue = listOf(12)
    private val startObjectValue = Rect(0, 0, 2, 2)

    private val uniqueIntLiveData = spy(UniqueLiveData(12))
    private val uniqueCollectionLiveData = spy<UniqueLiveData<List<Int>>>()
    private val uniqueObjectLiveData = spy<UniqueLiveData<Rect>>()

    @BeforeEach
    fun clearTestComponents() {
        changes = 0
    }

    @Test
    fun runActionOnUniqueValueOnlyTest() {

        uniqueIntLiveData.observeForever {
            changes += 1
        }
        uniqueIntLiveData.value = 12
        uniqueIntLiveData.value = 15
        assertEquals(1, changes)
    }

    @Test
    fun valueCollectionTest() {

        uniqueCollectionLiveData.observeForever {
            changes += 1
        }
        uniqueCollectionLiveData.value = startCollectionValue
        uniqueCollectionLiveData.value = startCollectionValue.toList()
        assertEquals(1, changes)
    }

    @Test
    fun valueObjectTest() {

        uniqueObjectLiveData.observeForever {
            changes += 1
        }
        uniqueObjectLiveData.value = startObjectValue
        uniqueObjectLiveData.value = Rect(startObjectValue)
        assertEquals(2, changes)
    }
}
