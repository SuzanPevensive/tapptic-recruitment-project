package kowaliszyn.zuzanna.tapptic.test.utils.liveData

import com.nhaarman.mockitokotlin2.spy
import kowaliszyn.zuzanna.tapptic.jUnitExtensions.InstantExecutorExtension
import kowaliszyn.zuzanna.tapptic.utils.liveData.SwitchableLiveData
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class)
class SwitchableLiveDataUnitTest {

    private var changes = 0

    private val switchableLiveData = spy<SwitchableLiveData>()

    @BeforeEach
    fun clearTestComponents() {
        changes = 0
    }

    @Test
    fun runActionOnSwitchOnlyTest() {

        switchableLiveData.observeForever {
            changes += 1
        }
        switchableLiveData.value = false
        switchableLiveData.value = false
        switchableLiveData.value = true
        switchableLiveData.value = false
        switchableLiveData.switch()

        assertEquals(true, switchableLiveData.value)
        assertEquals(4, changes)
    }
}
