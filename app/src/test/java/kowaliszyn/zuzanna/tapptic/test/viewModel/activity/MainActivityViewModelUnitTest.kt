package kowaliszyn.zuzanna.tapptic.test.viewModel.activity

import kowaliszyn.zuzanna.tapptic.jUnitExtensions.InstantExecutorExtension
import kowaliszyn.zuzanna.tapptic.mock.viewModel.MainActivityViewModelMock
import kowaliszyn.zuzanna.tapptic.model.data.ItemData
import kowaliszyn.zuzanna.tapptic.sealeds.ItemState
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class)
class MainActivityViewModelUnitTest {

    private val mainActivityViewModel = MainActivityViewModelMock.getInstance()

    private val itemsList = listOf(
        ItemData(
            "11",
            "11",
            "11"
        ),
        ItemData(
            "12",
            "12",
            "12"
        )
    )

    @BeforeEach
    fun setAdapterData() {
        mainActivityViewModel.adapter.setData(itemsList)
    }

    @Test
    fun getSelectedOrFirstItemNameTest() {
        mainActivityViewModel.getSelectedOrFirstItemName()
        assertEquals(true, mainActivityViewModel.adapter.itemState is ItemState.Selected)
        assertEquals(itemsList.first().name, mainActivityViewModel.adapter.itemState?.name)
        mainActivityViewModel.itemTapName.value = itemsList[1].name
        mainActivityViewModel.getSelectedOrFirstItemName()
        assertEquals(itemsList[1].name, mainActivityViewModel.adapter.itemState?.name)
    }

    @Test
    fun selectItem() {
        mainActivityViewModel.selectItem(itemsList.first().name)
        assertEquals(true, mainActivityViewModel.adapter.itemState is ItemState.Selected)
        assertEquals(itemsList.first().name, mainActivityViewModel.adapter.itemState?.name)
    }

    @Test
    fun unSelectItem() {
        mainActivityViewModel.unSelectItem(itemsList[1].name)
        assertEquals(true, mainActivityViewModel.adapter.itemState is ItemState.Focused)
        assertEquals(itemsList[1].name, mainActivityViewModel.adapter.itemState?.name)
    }

    @Test
    fun onSaveState() {
        mainActivityViewModel.onSaveState()
        assertEquals(itemsList, mainActivityViewModel.listSavedData)
    }
}
