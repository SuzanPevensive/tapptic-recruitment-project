package kowaliszyn.zuzanna.tapptic.test.utils.liveData

import kowaliszyn.zuzanna.tapptic.jUnitExtensions.InstantExecutorExtension
import kowaliszyn.zuzanna.tapptic.utils.liveData.SingleEventLiveData
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class)
class SingleEventLiveDataUnitTest {

    private val singleEventLiveData = SingleEventLiveData()

    @Test
    fun runEventTest() {

        var someValue = 17
        singleEventLiveData.observeForever {
            someValue -= 1
        }

        singleEventLiveData.run()
        singleEventLiveData.run()

        assertEquals(16, someValue)
    }
}
