package kowaliszyn.zuzanna.tapptic.test.conventer

import kowaliszyn.zuzanna.tapptic.const.*
import kowaliszyn.zuzanna.tapptic.mock.converter.DataConverterMock
import kowaliszyn.zuzanna.tapptic.model.data.ItemData
import kowaliszyn.zuzanna.tapptic.model.response.converted.ItemResponse
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class DataConverterUnitTest {

    private val dataConverter = DataConverterMock.getInstance()

    @Test
    fun convertEventsResponseTest() {

        val name = DefaultsTestConst.STRING
        val text = DefaultsTestConst.STRING
        val image = DefaultsTestConst.STRING

        val eventDto = ItemResponse(
            name,
            text,
            image
        )
        val event = ItemData(
            name,
            text,
            image,
        )
        dataConverter.apply {
            assertEquals(listOf(event), convertListItemsResponse(listOf(eventDto)))
        }
    }
}
