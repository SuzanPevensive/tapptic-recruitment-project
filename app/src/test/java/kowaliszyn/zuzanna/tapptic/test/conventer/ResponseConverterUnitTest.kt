package kowaliszyn.zuzanna.tapptic.test.conventer

import kowaliszyn.zuzanna.tapptic.const.*
import kowaliszyn.zuzanna.tapptic.mock.converter.ResponseConverterMock
import kowaliszyn.zuzanna.tapptic.model.response.converted.ItemResponse
import kowaliszyn.zuzanna.tapptic.model.response.dto.ItemResponseDto
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ResponseConverterUnitTest {

    private val responseConverter = ResponseConverterMock.getInstance()

    @Test
    fun convertEventsResponseTest() {

        val name = DefaultsTestConst.STRING
        val text = DefaultsTestConst.STRING
        val image = DefaultsTestConst.STRING

        val eventDto = ItemResponseDto(
            name,
            text,
            image
        )
        val event = ItemResponse(
            name,
            text,
            image
        )
        responseConverter.apply {
            assertEquals(listOf(event), convertListItemsResponse(listOf(eventDto)))
        }
    }
}
