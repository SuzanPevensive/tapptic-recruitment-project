package kowaliszyn.zuzanna.tapptic.mock.manager

import com.nhaarman.mockitokotlin2.spy
import kowaliszyn.zuzanna.tapptic.manager.impl.ErrorManagerImpl

object ErrorManagerMock {

    fun getInstance(): ErrorManagerImpl = spy()
}
