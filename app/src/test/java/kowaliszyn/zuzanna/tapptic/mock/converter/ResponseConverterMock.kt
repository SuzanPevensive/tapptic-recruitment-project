package kowaliszyn.zuzanna.tapptic.mock.converter

import kowaliszyn.zuzanna.tapptic.converter.ResponseConverter

object ResponseConverterMock {
    fun getInstance() = ResponseConverter(DataConverterMock.getInstance())
}
