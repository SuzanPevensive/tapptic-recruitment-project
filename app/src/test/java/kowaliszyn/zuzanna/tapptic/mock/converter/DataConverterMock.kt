package kowaliszyn.zuzanna.tapptic.mock.converter

import kowaliszyn.zuzanna.tapptic.converter.DataConverter

object DataConverterMock {
    fun getInstance() = DataConverter()
}
