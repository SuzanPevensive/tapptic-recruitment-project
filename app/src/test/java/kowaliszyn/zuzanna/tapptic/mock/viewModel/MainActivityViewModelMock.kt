package kowaliszyn.zuzanna.tapptic.mock.viewModel

import com.nhaarman.mockitokotlin2.mock
import kowaliszyn.zuzanna.tapptic.viewModel.activity.MainActivityViewModel

object MainActivityViewModelMock {
    fun getInstance() = MainActivityViewModel(mock { }, mock { })
}
